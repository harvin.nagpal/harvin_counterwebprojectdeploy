artifactory_url="http://localhost:8081/artifactory"

repo="libs-release-local"
artifacts="com/qaagility/CounterWebApp"

url=$artifactory_url/$repo/$artifacts

file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/maven-metadata.xml | grep '<release>' |head -1 | sed "s/.*<release>\([^<]*\)<\/release>.*/\1/"`

BUILD_LATEST="$url/$version/CounterWebApp-$build.war"
echo "File Name  = " +$BUILD_LATEST

echo $BUILD_LATEST > filename.txt
